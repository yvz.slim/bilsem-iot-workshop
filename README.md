# Sıcaklık ve nem  sensörü ile bir görsel uyarı sistemi yapımı

Bu repo, 27 ve 28 Ocak 2021 tarihinde Marmara Üniversitesi bünyesinde yer alan [MInD-NET Araştırma Laboratuarı](https://www.mind-net.org) 'nda faaliyet gösteren Yavuz Selim Bostancı tarafından; [İstanbul Fuat Sezgin Bilim ve Sanat Merkezi](http://istanbulbilsem.meb.k12.tr/) 'nin hazırladığı programda gerçekleştirilen atölye çalışmasını içermektedir.

---

1. Raspberry PI cihazına Rasbian işletim sistemi kurunuz.
   
   > varsayılan kullanıcı adı "pi" ve şifresi "raspberry"dir.

2. Bir terminal dosyası açınız sırasıyla aşağıdaki komutları çalıştırarak cihazınızı güncelleyiniz ve gerekli paketleri yükleyiniz.

> sudo apt-get update

> sudo apt-get install git
> 
> sudo apt-get install python3-venv

3. Klasör yapısı ve gereksinimler dosyasını indirmek için masaüstünde bir terminal açarak "git " ile başlayan komutları çalıştırınız.
   
   > Terminalde "cd" komutunu kullanarak klasörler arasında geçiş yapabiliyorsunuz. Terminal açtıktan sonra "cd Desktop" komutunu yazıp enter'a bastığınızda çalıştığınız klasörü Masaüstü olarak değiştirebilirsiniz.

[Yavuz Selim Bostancı / bilsem-iot-workshop · GitLab](https://gitlab.com/yvz.slim/bilsem-iot-workshop)

Git programını yükledikten sonra terminalde aşağıdaki komutları çalıştırarak kendi isminiz ve email adresinize göre konfigürasyon yapınız.

> git config --global user.name "adiniz"
> 
> git config --global user.email "[eposta@example.com](mailto:eposta@example.com)"

Aşağıdaki komutu terminalde çalıştırarak bu sitedeki dosyaları ve kodları bilgisayarınızda indirebilirsiniz. Bu komutu terminaliniz "Desktop" ayarlı şekilde çalıştırdığınızda masaüstünüzde "bilsem-iot-workshop" adında bir klasör, ve içinde klasör ve kod dosyaları oluşacaktır.

> git clone [Yavuz Selim Bostancı / bilsem-iot-workshop · GitLab](https://gitlab.com/yvz.slim/bilsem-iot-workshop.git)

4. terminaliniz "Desktop" konumunda iken
   
   > **cd bilsem-iot-workshop**
   
   komutunu çalıştırınız ve aşağıdaki kodu da çalıştırarak bir sanal ortam oluşturunuz.

> python3 -m venv sanalortam

Bu

5. Aşağıdaki komut ile sanal ortamı aktifleştiriniz. Sanal ortam aktifleştirildiğinde terminalinizin satır başında **(sanalortam)** ibaresi görülecektir.

> source sanalortam/bin/activate

6. Gerekli python kütüphanelerini aşağıdaki komut vasıtasıyla yükleyiniz.

> pip3 install -r gereksinimler.txt

7. Raspberry PI, sensör cihazı ve led'leri jumper kablolar ve breadboard yardımıyla aşağıdaki şemaya göre bağlayınız.
   
   <img src="Dosyalar/Setup.png" title="" alt="Setup.png" width="603">

--- 

## Sensör ve Led Kodları

1. Öncelikle *sensorler* klasörünün içinde **sensor_ve_led.py** adında bir dosya oluşturuyoruz ve bu dosyaya sağ tıklayarak "Open With" seçeneğinin yanındaki Thonny Python IDE yardımıyla açıyoruz.

2. Sensörden değer okumak için gerekli modülleri import ediyoruz.
   
   ```py
   from dht11 import DHT11
   import RPi.GPIO as GPIO 
   ```

3. Yukarıdaki şemada bağladığınız GPIO pinlerinin tanımlamalarını yapıyoruz.
   
   ```py
   kirmizi_led_pin = 2
   mavi_led_pin = 3
   sensor = DHT11(pin=4)
   
   GPIO.setmode(GPIO.BCM)
   GPIO.setup(kirmizi_led_pin, GPIO.OUT)
   GPIO.setup(mavi_led_pin, GPIO.OUT)
   ```

4. Sıcaklık ve nem sensöründen veri okumak için bir fonksiyon yazıyoruz. Bu fonksiyon sensörden okuduğu veriyi bir değer objesi olarak döndürecek ve biz bu objenin *sicaklik* ve *nem* değişkenlerini kullanarak değeri kullanabileceğiz.
   
   ```python
   def dht11_oku():
     class Deger:
         sicaklik = 0
         nem = 0
   
     sonuc = sensor.read()
     if sonuc.is_valid():
         deger = Deger()
         deger.sicaklik = sonuc.temperature
         deger.nem = sonuc.humidity
         return deger
   ```

5. Ledlerimizi kontrol etmek için gerekli *class* larımızı ve ledleri yakıp söndürmek için gerekli fonksiyonlarımızı oluşturalım. Bu projede bir kırmızı bir de mavi led kullanıyoruz.
   
   ```python
   class KirmiziLed:
     def yak(self):
         GPIO.output(kirmizi_led_pin, True)
   
     def sondur(self):
         GPIO.output(kirmizi_led_pin, False)
   
   class MaviLed:
     def yak(self):
         GPIO.output(mavi_led_pin, True)
   
     def sondur(self):
         GPIO.output(mavi_led_pin, False)
   ```

6. Şimdi de bu fonksiyonlarımızı kullanarak bir demo uygulama yapalım. Öncelikle yine *sensorler* klasörünün içerisinde **sensor_demo.py** adında bir dosya oluşturarak Thonny Python IDE yardımıyla açalım ve gerekli modülleri import edelim.
   
   Bu demoda ledler yardımıyla sıcaklık ve nem değerlerine bağlı bir görsel uyarı sistemi yapacağız. Saniyede bir defa ölçüm yapacağız. Sıcaklık değeri belirli bir limiti geçtiğinde kırmızı led yanarak bizi uyaracak ve Nem değeri belirli bir limiti geçtiğinde mavi led yanarak bizi uyaracak. Değerler limitimizin altında kaldığında da ledlerimiz sönecek.
   
   ```python
   import sensor_ve_led
   import time
   ```

7. Yukarıda yazdığımız *class* lardan birer led objeleri oluşturalım, ve sıcaklık ve nem limitlerini belirleyelim.
   
   ```python
   kirmizi_led = sensor_ve_led.KirmiziLed()
   mavi_led = sensor_ve_led.MaviLed()
   sicaklik_limit_deger = 15
   nem_limit_deger = 70
   ```

8. Bir döngü içinde değerleri kontrol ederek ledlerimizi yakıp söndürelim. Aynı zamanda da sıcaklık ve nem değerlerini konsolumuza yazdıralım.
   
   ```python
   while True:
     deger = sensor_ve_led.dht11_oku()
     if deger:
         print("sicaklik:", deger.sicaklik, "nem:", deger.nem)
         if deger.sicaklik > sicaklik_limit_deger:
             kirmizi_led.yak()
         else:
             kirmizi_led.sondur()
         if deger.nem > nem_limit_deger:
             mavi_led.yak()
         else:
             mavi_led.sondur()
     time.sleep(1)
   ```

9. Thonny Python IDE üzerindeki "Çalıştır" tuşuna basarak programınızı çalıştırabilirsiniz. Sol altta nem ve sıcaklarınız görülecek ve limit değerlerinize göre ledleriniz yanıp sönecektir. Limit değerlerinizi değiştirerek veya ortamın sıcaklık ve nemini değiştirerek ledlerin davranışını gözlemleyebilirsiniz.
   
   > test etmek için sensörün üzerine ağzınızla sıcak hava üfleyebilirsiniz :)

İyi çalışmalar dilerim.

**Yavuz Selim Bostancı**

E-mail: y.bostanci@mind-net.org 
