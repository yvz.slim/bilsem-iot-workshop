import sensor_ve_led
import time

kirmizi_led = sensor_ve_led.KirmiziLed()
mavi_led = sensor_ve_led.MaviLed()
sicaklik_limit_deger = 15
nem_limit_deger = 70

while True:
    deger = sensor_ve_led.dht11_oku()
    if deger:    
        print("sicaklik", deger.sicaklik, "nem:", deger.nem)
        if deger.sicaklik > sicaklik_limit_deger:
            kirmizi_led.yak()
        else:
            kirmizi_led.sondur()
        if deger.nem > nem_limit_deger:
            mavi_led.yak()
        else:
            mavi_led.sondur()
        
    time.sleep(1)
