from dht11 import DHT11
import RPi.GPIO as GPIO 

kirmizi_led_pin = 2
mavi_led_pin = 3
sensor = DHT11(pin=4)

GPIO.setmode(GPIO.BCM)
GPIO.setup(kirmizi_led_pin, GPIO.OUT)
GPIO.setup(mavi_led_pin, GPIO.OUT)

def dht11_oku():
    class Deger:
        sicaklik = 0
        nem = 0
    
    sonuc = sensor.read()
    if sonuc.is_valid():
        deger = Deger()
        deger.sicaklik = sonuc.temperature
        deger.nem = sonuc.humidity
        return deger

class KirmiziLed:
    def yak(self):
        GPIO.output(kirmizi_led_pin, True)
    
    def sondur(self):
        GPIO.output(kirmizi_led_pin, False)
        
class MaviLed:
    def yak(self):
        GPIO.output(mavi_led_pin, True)
    
    def sondur(self):
        GPIO.output(mavi_led_pin, False)

